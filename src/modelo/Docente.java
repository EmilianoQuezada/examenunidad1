package modelo;

public class Docente {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoBase;
    private int horasImpartidas;

    public Docente() {
        this.numDocente = 0;
        this.nombre = "";
        this.domicilio = "";
        this.nivel = 0;
        this.pagoBase = 0.0f;
        this.horasImpartidas = 0;
    }

    public Docente(int numDocente, String nombre, String domicilio, int nivel, float pagoBase, int horasImpartidas) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBase = pagoBase;
        this.horasImpartidas = horasImpartidas;
    }
    
    public Docente(Docente aux) {
        this.numDocente = aux.numDocente;
        this.nombre = aux.nombre;
        this.domicilio = aux.domicilio;
        this.nivel = aux.nivel;
        this.pagoBase = aux.pagoBase;
        this.horasImpartidas = aux.horasImpartidas;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }

    public int getHorasImpartidas() {
        return horasImpartidas;
    }

    public void setHorasImpartidas(int horasImpartidas) {
        this.horasImpartidas = horasImpartidas;
    }
    
    public float calcularPago(){
        if(this.nivel==1){
            this.pagoBase += (this.pagoBase*.30);
            return this.pagoBase * this.horasImpartidas;
        }
        else if(this.nivel==2){
            this.pagoBase += (this.pagoBase*.50);
            return this.pagoBase * this.horasImpartidas;
        }
        else{
            this.pagoBase += (this.pagoBase*.100);
            return this.pagoBase * this.horasImpartidas;
        }
    }
    
    public float calcularBono(int hijos){
        if(hijos>=1 && hijos<=2){
            return (float)(this.calcularPago()*.05);
        }
        else if(hijos>=3 && hijos<=5){
            return (float)(this.calcularPago()*.10);
        }
        else if(hijos>5){
            return (float)(this.calcularPago()*.20);
        }
        else{
            return 0;
        }
    }
    
    public float calcularImpuesto(){
        return (float) (this.calcularPago() *.16);
    }
}
